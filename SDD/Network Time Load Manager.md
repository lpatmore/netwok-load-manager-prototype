# Network Scheduler SDD

## Contents

1. Revision History
2. Referenced Documents
3. Requirements Satisfied
4. Problem Description
5. Design Description
6. Design Views
7. Tests

## 1. Revision History

| Date  | Author  | Revision Summary  |
|---|---|---|
| 08/23/2019 | Landon Patmore | Initial draft. |
| 08/26/2019 | Landon Patmore | Changed NetworkTimeMessageType to sealed class objects. |
| 08/26/2019 | Landon Patmore | Updated variable and class names. |
| 08/28/2019 | Landon Patmore | Updated document with proper name, as well as redesigned entire implementation. |
| 09/03/2019 | Landon Patmore | Separated client and service code design. |
| 09/04/2019 | Landon Patmore | Final draft updates. |

## 2. Referenced Documents

- [08/19/2019 Meeting Notes](https://confluence.elsys.gtri.org/display/DALPX/2019-08-19+Network+Scheduler+Follow-Up+meeting)

## 3. Requirements Satisfied

- There are currently no requirements that this design satisfies as none have been written at this point in time.

## 4. Problem Description

*This does not include streamable data such as movies or audio.*

- All ITUs, at the time of writing this document, attempt to transfer data at the same time.  This causes the network to become saturated, as there is limited bandwidth on the aircraft network.  Thus, data transfer is both slow and can potentially, and most likely, drop since the network cannot handle all the traffic at once.

- There is no way to prioritize traffic for either certain types of data or groups of ITUs on a particular WAP.  This means that there is no way to properly utilize the limited network bandwidth on the aircraft.

- Data is only transferred at certain stages of flight.

### 4.1 Use cases

- When all ITUs come on the network at the same time, they all attempt to pre-cache at the same time.  Since there is limited network bandwidth, some ITUs may have their transfer connection cut.  Since our ITU code is not designed with pre-caching failures in mind, the pre-cache stops and does not attempt to restart.  This leads to a poor user experience when the user attempts to look through the movie catalog as the ITU must now retrieve the missing data, thus slowing the UI down.

- When third party applications are allowed to be loaded onto the ITUs, their network operations should not be able to initiate without first going through the Network Load Manager.  Third party applications, if not restricted, could potentially lead to poor network performance across the entire aircraft.  This service will only allow their network operations when their priority is allowed to.

## 5. Design Description

- This redesign is for handling RabbitMQ messages.

- This redesign is to help manage the transfer of data across the network.

- This redesign aims to to be able to transfer data anytime there is network bandwidth to do so, dictated by the server.  This allows network bandwidth to be utilized to its maximum potential while keeping data transfers from dropping or slowing to a halt.

- A service will be created to handle the sending and receiving of `NetworkTime` messages.

- The service will be created from within `deviceowner`.

  - The service will exist within its own process outside the scope of `deviceowner`.  This allows other applications to access it.

- The `AIDL` (Android Interface Definition Language) will be used as the communication medium between applications and the service.

  - This was decided so that when developers are implementing functionality to request network time from the server, they do not need to use hardcoded strings; rather they can use concrete function calls with callbacks.

- When applications request network time through the service, they will send a callback with their request.  This will allow the service to process the request, and wait for the server to respond back, and finally send it back to the requesting application.  It will be non-blocking on the requesting application side, as well as the service side.

### 5.1. Code Design

- The below sections describe how clients interact with the service and how the service interacts with the server.

- If a client requires a different priority at any point, it must request network time again with that new priority.  This will not impact the current state and/or transfer going on.

- `Request` and `Complete` are message types that the client sends to the service, which in turn the service sends to the server.

- `Start` and `Interrupt` are message types that the server sends to the service, which in turn the service sends to the client.

#### 5.1.1 Client Side

- The client sends messages to the service through the service's defined `AIDL` and binds to it.

##### 5.1.1.1 Request network time

The client calls the `requestNetworkTime` function with an `identifier`, `priority`, and `callback`.

- `identifier` : This is the package name of the application as this is unique to all applications on the ITU.
- `priority` : This is the priority code of the data that the ITU is transferring.
- `callback` : This is the callback that the service uses to communicate back to the client.

The client uses this function when it wants to request network time.  The client implements the callback with specific logic to handle when the service calls either an `onStart` or `onInterrupt` inside the callback.

##### 5.1.1.2 Complete network time

The client calls the `completeNetworkTime` function with an `identifier` and `priority`.

- `identifier` : This is the package name of the application as this is unique to all applications on the ITU.
- `priority` : This is the priority code of the data that the ITU is transferring.

The client uses this function when it is done transferring data to the server.

#### 5.1.2 Service Side

- The service sends messages to the server.

##### 5.1.2.1. Request network time

The service will send out a message with a `seatId`, `wapIp`, and requested `priority` key included.

- `seatId` : This is the seat id of the ITU itself.
- `wapIp` : This is the ip address of the WAP that the ITU is currently connected to.
- `priority` : This is the priority code of the data that the ITU is transferring.

The service is now waiting for a response from the server to proceed.  This waiting period can be an indeterminate amount of time.  During this time, the client that has requested to transfer data is not transferring data of that priority since it does not have permission to do so.

##### 5.1.2.2. Start network time

The service will receive a message with its `seatId` and the requested `priority` key included.

- `seatId` : This is the seat id of the ITU itself.
- `priority` : This is the priority code of the data that the ITU is transferring.

The service now has now received permission and will send a start message to the client to begin transferring data to/from the server with the specified priority.

##### 5.1.2.3. Complete network time

The service will send out a message with its `seatId` and requested `priority` key included.

- `seatId` : This is the seat id of the ITU itself.
- `priority` : This is the priority code of the data that the ITU is transferring.

The service sends this message once the client is done transferring data to the server and the client has called the `completeTime` function.  It now gives up its permission to transfer data of that priority level.  If the client wants to transfer data again, it must request network time to do so through the service.

##### 5.1.2.4. Interrupt network time

The service will receive a message with its`seatId` and requested `priority` key included.

- `seatId` : This is the seat id of the ITU itself.
- `priority` : This is the priority code of the data that the ITU is transferring.

The service must tell the client to immediately stop transferring data of the specified priority.  The client does not need to requeue with the server since the server will requeue the ITU automatically.  This means that the ITU is in the waiting state, as if it has just requested network time.

## 6. Design Views

The below diagram(s) show how the the Network Load Manager will work at a higher level.

### 6.1 Flow

- If a `request message` is sent, no network transferring may transpire before a start message is sent back to the client.

- If a client is waiting for a `start message`, a `complete message` and/or `interrupt message` cannot be sent to the client before a `start message` is received.

- If a client has been interrupted, it must not request network time for the priority it was interrupted for.  The service will hold on to the original callback and the server will requeue the ITU.

### 6.2. Sequence Diagrams

- The below diagrams show the sequence of events that the Network Load Manager will perform in certain situations.

- Events that have filled in triangles are synchronous and half-filled triangles are asynchronous calls.

### 6.2.1. Sending requests

- The below diagram describes how the system will flow when a client requests to transfer data with the service, and the service interacting with the server.

![Request Time](images/requesttime.png)

- When requesting to bind to the service, the client must wait until the bind takes place to start interacting with the service.

- `requestTimeMessage` is asynchronous since the service does not notify the client that it got the request.  It is relying on the Android OS to make sure the service gets the request.  This is also due to the fact that the `requesTimeMessage` function on the service side must perform actions to send a RabbitMQ message to the server.  This may take an arbitrary amount of time, thus it could possibly block the client.  This is not the desired behavior.

### 6.2.2 Receiving responses

- The below diagrams describe how the system will flow when the server responds back to the service, and the service interacting with the client.

#### 6.2.2.1 Complete time

![Request Time](images/completetime.png)

- The callback `onStart` is asynchronous since the service must not be blocked when the client is performing actions.  It returns immediately.

- Transferring of files can happen starting from the client or the server.  This is determined by both the client and server when transferring begins.

- `completeMessage` is asynchronous since the service does not notify the client that it got the request.  It is relying on the Android OS to make sure the service gets the request.  This is also due to the fact that the `completeMessage` function on the service side must perform actions to send a RabbitMQ message to the server.  This may take an arbitrary amount of time, thus it could possibly block the client.  This is not the desired behavior.

#### 6.2.2.1 Interrupted Time

![Request Time](images/interrupttime.png)

- This diagram is the same as the above `Complete Time` diagram, with the addition of an interrupt being sent from the server.

- When an interrupt occurs, the service immediately sends this to the client to tell it to stop transferring files.

- The client must stop transferring files immediately.

- The callback `onInterrupt` is asynchronous since the service must not be blocked when the client is performing actions.  It returns immediately.

## 7. Tests

- A prototype has been built: [Network Load Manager Prototype](https://bitbucket.org/lpatmore/netwok-load-manager-prototype/src/master/)
