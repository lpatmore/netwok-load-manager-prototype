package com.example.servicetestapplication

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.service.network.NetworkTimeCallback
import com.service.network.NetworkTimeInterface
import kotlin.random.Random

class SuperCoolService : Service() {
    private val listOfCallBacks = mutableMapOf<Int, MutableMap<String, NetworkTimeCallback>>()

    init {
        randomlySendBackStartMessage()
    }

    /**
     * Test function that randomly picks a priority to send the start message to.
     */
    private fun randomlySendBackStartMessage() {
        Thread(Runnable {
            while (true) {
                Thread.sleep(5000)

                Log.d(TAG, "Starting a priority!")

                val rand = Random(System.currentTimeMillis()).nextInt(3)

                Log.d(TAG, "Starting priority $rand...")

                listOfCallBacks[rand]?.forEach {
                    it.value.onStart()
                }

                Log.d(TAG, "List of callbacks: $listOfCallBacks")
            }
        }).start()
    }

    private val bind = object : NetworkTimeInterface.Stub() {
        /**
         * Remote function call from the client.  This adds the request to the internal data
         * structure.
         * @param identifier String client package name
         * @param priority Int priority of the files requested to transfer
         * @param callback NetworkTimeCallback callback client uses to receive information
         * from the service
         */
        override fun requestTimeMessage(
            identifier: String,
            priority: Int,
            callback: NetworkTimeCallback
        ) {
            Log.d(TAG, "Request message received, handling...")

            listOfCallBacks[priority]?.let {
                Log.d(
                    TAG,
                    "Priority $priority exists..."
                )
                if (it.containsKey(identifier)) {
                    Log.w(TAG, "$priority already contains a callback for identifier $identifier")
                }

                it[identifier] = callback
            } ?: run {
                Log.d(
                    TAG,
                    "Priority $priority does not exist...creating list of priority $priority callbacks."
                )
                listOfCallBacks[priority] = mutableMapOf(Pair(identifier, callback))
            }

            Log.d(TAG, "List of callbacks: $listOfCallBacks")
        }

        /**
         * Remote function call from the client.  Removes the callback for the identifier
         * and priority specified by the client.  If an identifier exists but the priority
         * does not exist, or vice-a-versa, then no callbacks are removed.
         * @param identifier String client package name
         * @param priority Int priority of the files that were transferred
         */
        override fun completeMessage(identifier: String, priority: Int) {
            Log.d(TAG, "Complete message received, handling...")

            listOfCallBacks[priority]?.let {
                it[identifier]?.let { _ ->
                    it.remove(identifier)
                } ?: run {
                    Log.w(
                        TAG,
                        "Identifier $identifier does not have any callbacks."
                    )
                }
            } ?: run {
                Log.w(
                    TAG,
                    "Priority $priority does not have any callbacks."
                )
            }

            Log.d(TAG, "List of callbacks: $listOfCallBacks")
        }

        /**
         * Remote testing function call from the client.  This spoofs an interrupt message
         * coming from the server.  It randomly selects a priority to interrupt and then it
         * interrupts all callbacks within that priority.  It does NOT remove the callbacks
         * from the internal data structure since the server requeues the ITU for that
         * specific priority, thus the client does NOT need to request again for an interrupted
         * priority.
         */
        override fun spoofServerInterruptMessage() {
            Log.d(TAG, "Interrupting a priority!")

            val rand = Random(System.currentTimeMillis()).nextInt(3)

            Log.d(TAG, "Interrupting priority $rand...")

            listOfCallBacks[rand]?.forEach {
                it.value.onInterrupt()
            }

            Log.d(TAG, "List of callbacks: $listOfCallBacks")
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "Service started!")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(p0: Intent?): IBinder? {
        return bind
    }

    companion object {
        private const val TAG = "SuperCoolService"
    }
}
