// NetworkTimeInterface.aidl
package com.service.network;

// Declare any non-default types here with import statements

interface NetworkTimeCallback {
    void onStart();

    void onInterrupt();
}
