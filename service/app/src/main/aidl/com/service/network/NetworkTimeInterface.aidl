// NetworkTimeInterface.aidl
package com.service.network;

// Declare any non-default types here with import statements
import com.service.network.NetworkTimeCallback;

interface NetworkTimeInterface {
    void requestTimeMessage(String identifier, int priority, NetworkTimeCallback callback);

    void completeMessage(String identifier, int priority);

    void spoofServerInterruptMessage();
}
