package com.example.clienttestapplication

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity;
import com.service.network.NetworkTimeCallback
import com.service.network.NetworkTimeInterface
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ServiceConnection {

    private var service: NetworkTimeInterface? = null

    override fun onServiceDisconnected(p0: ComponentName?) {
        Toast.makeText(this, "Disconnected from NetworkTime Scheduler service.", Toast.LENGTH_SHORT)
            .show()

        // Continuously tries to rebind to the service.
        // TODO: Probably should have a back off strategy implemented here
        applicationContext.bindService(
            Intent("com.example.servicetestapplication.SuperCoolService").setPackage("com.example.servicetestapplication")
            , this, BIND_AUTO_CREATE
        )

    }

    override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
        Toast.makeText(this, "Connected to NetworkTime Scheduler service.", Toast.LENGTH_SHORT)
            .show()
        service = NetworkTimeInterface.Stub.asInterface(p1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        applicationContext.bindService(
            Intent("com.example.servicetestapplication.SuperCoolService").setPackage("com.example.servicetestapplication")
            , this, BIND_AUTO_CREATE
        )

        requestTimeMessageClickListener(p0, t0, 0)
        requestTimeMessageClickListener(p1, t1, 1)
        requestTimeMessageClickListener(p2, t2, 2)

        completeMessageClickListener(c0, t0, 0)
        completeMessageClickListener(c1, t1, 1)
        completeMessageClickListener(c2, t2, 2)

        interrupt.setOnClickListener {
            service?.spoofServerInterruptMessage()
        }
    }

    /**
     * Sets a button's click listener to send a `requestTimeMessage` to the service.  A callback is created
     * that is sent to the service.  This callback receives `onStart` and `onInterrupt` function calls and simulates
     * what the client should do.
     * @param button Button button to set click listener for
     * @param text TextView textview to change text of
     * @param priority Int priority of the files to transfer
     */
    private fun requestTimeMessageClickListener(button: Button, text: TextView, priority: Int) {
        button.setOnClickListener {
            val cb = object : NetworkTimeCallback.Stub() {
                /**
                 * This function is called by the service when the server is ready to transfer files with the ITU.
                 */
                override fun onStart() {
                    runOnUiThread {
                        text.text = "Priority $priority: Started!"
                    }
                }

                /**
                 * This function is called by the service when the server must stop the transferring of files
                 * with the ITU.
                 */
                override fun onInterrupt() {
                    runOnUiThread {
                        text.text = "Priority $priority: Interrupted!"
                    }
                }
            }

            service?.requestTimeMessage(application.packageName, priority, cb)
        }
    }

    /**
     * Sets a button's click listener to send a `completeMessage` to the service.
     * @param button Button button to set click listener for
     * @param text TextView textview to change text of
     * @param priority Int priority of the files to transfer
     */
    private fun completeMessageClickListener(button: Button, text: TextView, priority: Int) {
        button.setOnClickListener {
            service?.completeMessage(application.packageName, priority)
            text.text = "Priority $priority: Complete!"
        }

    }

    companion object {
        private const val TAG = "Client"
    }
}
