# Netwok Load Manager Prototype

This demonstrates one application starting a service, and another being able to communicate with that service using `AIDL` (Android Interface Definition Language).

## To Run

1. In Android Studio, edit the build configuration of the `server` application and change `Launch` from the default to `Nothing` and then run it
2. Using adb, run `adb shell am startservice -n com.example.servicetestapplication/.SuperCoolService` (you probably don't have to do this as just putting the app on the ITU seems to run it automatically)
3. Run the `client` application and test the buttons

If you open up logcat and find the process that has `:remote` appended to it, that is the service and you will be able to see what it is doing.  

The service sleeps for 5 seconds to send back a start message to simulate the server waiting for bandwith to free up to allow the ITU to start transfering data.

All functions are logged as things happens so you can see what the service is doing.